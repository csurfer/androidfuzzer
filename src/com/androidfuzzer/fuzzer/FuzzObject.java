package com.androidfuzzer.fuzzer;

/**
 * Interface for fuzz object. This object can be associated with an event and
 * the fuzz method on the event will trigger fuzzMe method of the class 
 * implementing this interface.
 */
public interface FuzzObject {
	
	/**
	 * Operation for this fuzzer object.
	 */
	public void fuzzMe(FuzzerHelper fuzzerHelper);

}
