package com.androidfuzzer.fuzzer;

import com.example.ss.FlowContext;

import au.com.ds.ef.Event;
import au.com.ds.ef.FlowBuilder;
import au.com.ds.ef.State;
import au.com.ds.ef.TransitionBuilder;

/**
 * Wrapper around the Event object. This class encapsulates a fuzzer object
 * which can be called as side effect of an event.
 */
public class FuzzEvent{
	private FuzzObject fuzzer;
	private Event<FlowContext> event;
	
	/**
	 * Public contructor which intantiates an event object with this class and
	 * initializes this with a fuzz object.
	 * @param fuzzObject
	 */
	public FuzzEvent(FuzzObject fuzzObject){
		fuzzer = fuzzObject;
		event = FlowBuilder.event();
	}
	
	/**
	 * Public constructor used when there is no fuzz object associated with this event.
	 * This is the case when an event is used only for the state transition but not for
	 * fuzzing objects.
	 * 
	 */
	public FuzzEvent(){
		event = FlowBuilder.event();
	}
	
	/**
	 * Fuzz the fuzzObject.
	 * 
	 * @param fuzzerHelper Helper object used by the fuzzer.
	 */
	public void fuzz(FuzzerHelper fuzzerHelper){
		if(fuzzer == null){
			throw new IllegalArgumentException("Event not initialized with a fuzzer object");
		}
		fuzzer.fuzzMe(fuzzerHelper);
	}	
	
	/**
	 * Trigger state transition on the Event object encapsulated within this object.
	 * 
	 * @param context Flow context
	 */
	public void trigger(FlowContext context){
		event.trigger(context);
	}
	
	/**
	 * To transition for the event embedded in this fuzz event.
	 * 
	 * @param context Flow context
	 * @return A transition builder object
	 */
	public TransitionBuilder<FlowContext> to(State<FlowContext> context){
		return event.to(context);		
	}
	
	
	/**
	 * End transition for the event embedded in this fuzz event.
	 * 
	 * @param context Flow context
	 * @return A transition builder object
	 */
	public TransitionBuilder<FlowContext> finish(State<FlowContext> context){
		return event.finish(context);		
	}

}



