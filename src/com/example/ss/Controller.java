package com.example.ss;

import android.os.Bundle;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.util.Log;

public class Controller {
	
	private static void function1(MainActivity mainActivityObj, Bundle mainInstanceState) {
		ConfigurationFuzzer configurationFuzzer =  new ConfigurationFuzzer(mainActivityObj);
		Context context = mainActivityObj.getApplicationContext(); 
		configurationFuzzer.fuzzAllManagers(context, mainActivityObj);
	}
	
	private static void function2(MainActivity mainActivityObj, Bundle mainInstanceState) {
		System.out.println("Vinyas Method.");
	}

	private static void function3(MainActivity mainActivityObj, Bundle mainInstanceState) {
		//ToastCheck tObj = new ToastCheck();
		//tObj.start(mainActivityObj);
		WifiCheck wObj = new WifiCheck(mainActivityObj);
		/*
		 * State to fuzz the wifi apk in
		 * 0-4 Corresponding to Disabled Enabled etc 
		 * and 5 corresponding to any to create randomness.
		 */
		wObj.start(5);
	}
	
	
	public static void callThis(MainActivity mainActivityObj, Bundle mainInstanceState) {
		int control = 3;
		switch(control) {
			case 1:
				//Soujanya
				function1(mainActivityObj, mainInstanceState);
				break;
			case 2:
				//Vinyas
				function2(mainActivityObj, mainInstanceState);
				break;
			case 3:
				//Vishwas
				function3(mainActivityObj, mainInstanceState);
				break;
			default:
				break;
		}
	}
}
