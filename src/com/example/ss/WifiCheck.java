package com.example.ss;
import java.lang.reflect.Method;
import java.lang.reflect.Method.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import com.androidfuzzer.fuzzer.FuzzerHelper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.*;
import android.util.Log;
import android.util.Log.*;

public class WifiCheck {
	
	private WifiManager mWifiManager;
	private MainActivity mMainActivity;
	private FuzzerHelper mWifiFuzzerHelper;
	BroadcastReceiver mWifiStateChangedReceiver;
	private String LogTag;
	private int mStateToFuzz;
	
	WifiCheck(MainActivity mainActivityObj) {
		mMainActivity = mainActivityObj;
		mWifiManager = (WifiManager)mMainActivity.getSystemService(mMainActivity.getApplicationContext().WIFI_SERVICE);
		mWifiFuzzerHelper = new FuzzerHelper(WifiManager.class, mWifiManager);
		LogTag = "WifiCheck";
		/*
		 * Registering the receiver to make sure that we start fuzzer in every state that Wifi
		 * can be at during turning on or turning off.
		 */
		mWifiStateChangedReceiver = new BroadcastReceiver() {
		    @Override
		    public void onReceive(Context context, Intent intent)
		    {
		        // TODO Auto-generated method stub
		        int extraWifiState = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, WifiManager.WIFI_STATE_UNKNOWN);
		        switch (extraWifiState)
		        {
		        	case WifiManager.WIFI_STATE_DISABLED:
		        		Log.i(LogTag,"Disabled");
		        		fuzzInWifiState(WifiManager.WIFI_STATE_DISABLED);
		        		break;
		        	case WifiManager.WIFI_STATE_DISABLING:
		        		Log.i(LogTag,"Disabling");
		        		fuzzInWifiState(WifiManager.WIFI_STATE_DISABLING);
		        		break;
		        	case WifiManager.WIFI_STATE_ENABLED:
		        		Log.i(LogTag,"Enabled");
		        		fuzzInWifiState(WifiManager.WIFI_STATE_ENABLED);
		        		break;
		        	case WifiManager.WIFI_STATE_ENABLING:
		        		Log.i(LogTag,"Enabling");
		        		fuzzInWifiState(WifiManager.WIFI_STATE_ENABLING);
		        		break;
		        	case WifiManager.WIFI_STATE_UNKNOWN:
		        		Log.i(LogTag,"Unknown");
		        		fuzzInWifiState(WifiManager.WIFI_STATE_UNKNOWN);
		        		break;
		        	default:
		        		Log.i(LogTag,"Error");
		        		break;
		        }
		    }
		};
		mMainActivity.registerReceiver(mWifiStateChangedReceiver,new IntentFilter(WifiManager.WIFI_STATE_CHANGED_ACTION));
	}
	
	@Override
	protected void finalize() throws Throwable {
		// TODO Auto-generated method stub
		mMainActivity.unregisterReceiver(mWifiStateChangedReceiver);
		super.finalize();
	}
	
	public void fuzzInWifiState(int state){
		if(state == mStateToFuzz || mStateToFuzz == 5){
			try {
				fuzzWifi();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void fuzzWifi() throws Exception{
		/*
		 * Wifi Configurations
		 */
		List<WifiConfiguration>	 configs = mWifiManager.getConfiguredNetworks();
		Collections.shuffle(configs);
		
		/*
		 * Bool value arrays
		 */
		List<Boolean> tfArray = new ArrayList<Boolean>();
		tfArray.add(true);
		tfArray.add(false);
		
		/*
		 * Random number generator
		 */
		Random rand = new Random();
		
		/*
		 * Lock types
		 */
		List<Integer> lockTypes = new ArrayList<Integer>();
		lockTypes.add(mWifiManager.WIFI_MODE_FULL);
		lockTypes.add(mWifiManager.WIFI_MODE_FULL_HIGH_PERF);
		lockTypes.add(mWifiManager.WIFI_MODE_SCAN_ONLY);
		
		for (Method m : mWifiFuzzerHelper.methods)
		{
			if(m.getName().equals("addNetwork")){
				mWifiFuzzerHelper.fuzzMethodWithParameters(m, configs.get(rand.nextInt(configs.size())));
			}
			else if(m.getName().equals("calculateSignalLevel")){
				mWifiFuzzerHelper.fuzzMethodWithParameters(m, mWifiFuzzerHelper.constants.getValueOfType(Integer.class, tfArray.get(rand.nextInt(tfArray.size()))));
			}
			else if(m.getName().equals("calculateSignalLevel")){
				mWifiFuzzerHelper.fuzzMethodWithParameters(m, mWifiFuzzerHelper.constants.getValueOfType(Integer.class, tfArray.get(rand.nextInt(tfArray.size()))), mWifiFuzzerHelper.constants.getValueOfType(Integer.class, tfArray.get(rand.nextInt(tfArray.size()))));
			}
			else if(m.getName().equals("compareSignalLevel")){
				mWifiFuzzerHelper.fuzzMethodWithParameters(m, mWifiFuzzerHelper.constants.getValueOfType(Integer.class, tfArray.get(rand.nextInt(tfArray.size()))), mWifiFuzzerHelper.constants.getValueOfType(Integer.class, tfArray.get(rand.nextInt(tfArray.size()))));
			}
			else if(m.getName().equals("createMulticastLock")){
				mWifiFuzzerHelper.fuzzMethodWithParameters(m, mWifiFuzzerHelper.constants.getValueOfType(String.class, tfArray.get(rand.nextInt(tfArray.size()))));
			}
			else if(m.getName().equals("createWifiLock")){
				mWifiFuzzerHelper.fuzzMethodWithParameters(m, mWifiFuzzerHelper.constants.getValueOfType(String.class, tfArray.get(rand.nextInt(tfArray.size()))));
				mWifiFuzzerHelper.fuzzMethodWithParameters(m, lockTypes.get(rand.nextInt(lockTypes.size())), mWifiFuzzerHelper.constants.getValueOfType(String.class, tfArray.get(rand.nextInt(tfArray.size()))));
			}
			else if(m.getName().equals("disableNetwork")){
				mWifiFuzzerHelper.fuzzMethodWithParameters(m, configs.get(rand.nextInt(configs.size())).networkId);
			}
			else if(m.getName().equals("disconnect")){
				mWifiFuzzerHelper.fuzzMethod(m, tfArray.get(rand.nextInt(tfArray.size())));
			}
			else if(m.getName().equals("enableNetwork")){
				mWifiFuzzerHelper.fuzzMethodWithParameters(m, configs.get(rand.nextInt(configs.size())).networkId);
			}
			else if(m.getName().equals("getConnectionInfo")){
				mWifiFuzzerHelper.fuzzMethod(m, tfArray.get(rand.nextInt(tfArray.size())));
			}
			else if(m.getName().equals("getDhcpInfo")){
				mWifiFuzzerHelper.fuzzMethod(m, tfArray.get(rand.nextInt(tfArray.size())));
			}
			else if(m.getName().equals("getDhcpInfo")){
				mWifiFuzzerHelper.fuzzMethod(m, tfArray.get(rand.nextInt(tfArray.size())));
			}
			else if(m.getName().equals("getScanResults")){
				mWifiFuzzerHelper.fuzzMethod(m, tfArray.get(rand.nextInt(tfArray.size())));
			}
			else if(m.getName().equals("getWifiState")){
				mWifiFuzzerHelper.fuzzMethod(m, tfArray.get(rand.nextInt(tfArray.size())));
			}
			else if(m.getName().equals("isScanAlwaysAvailable")){
				mWifiFuzzerHelper.fuzzMethod(m, tfArray.get(rand.nextInt(tfArray.size())));
			}
			else if(m.getName().equals("isWifiEnabled")){
				mWifiFuzzerHelper.fuzzMethod(m, tfArray.get(rand.nextInt(tfArray.size())));
			}
			else if(m.getName().equals("pingSupplicant")){
				mWifiFuzzerHelper.fuzzMethod(m, tfArray.get(rand.nextInt(tfArray.size())));
			}
			else if(m.getName().equals("reassociate")){
				mWifiFuzzerHelper.fuzzMethod(m, tfArray.get(rand.nextInt(tfArray.size())));
			}
			else if(m.getName().equals("reconnect")){
				mWifiFuzzerHelper.fuzzMethod(m, tfArray.get(rand.nextInt(tfArray.size())));
			}
			else if(m.getName().equals("removeNetwork")){
				mWifiFuzzerHelper.fuzzMethodWithParameters(m, configs.get(rand.nextInt(configs.size())).networkId);
			}
			else if(m.getName().equals("saveConfiguration")){
				mWifiFuzzerHelper.fuzzMethod(m, tfArray.get(rand.nextInt(tfArray.size())));
			}
			else if(m.getName().equals("setTdlsEnabled")){
				mWifiFuzzerHelper.fuzzMethodWithParameters(m, mWifiFuzzerHelper.constants.getValidIpString(), tfArray.get(rand.nextInt(tfArray.size())));
			}
			else if(m.getName().equals("startScan")){
				mWifiFuzzerHelper.fuzzMethod(m, tfArray.get(rand.nextInt(tfArray.size())));
			}
			else{
				mWifiFuzzerHelper.fuzzMethodWithParameters(m, configs.get(rand.nextInt(configs.size())));
			}
		}
	}
	
	public void switchOnAndOff(){
		mWifiManager.setWifiEnabled(true);
		mWifiManager.setWifiEnabled(false);
	}
	
	public void start(int st){
		Log.i(LogTag,"Started");
		mStateToFuzz = st;
		//switchOnAndOff();
	}
}
