package com.example.ss.fuzzerObjects;

import com.androidfuzzer.fuzzer.FuzzObject;
import android.net.wifi.WifiConfiguration;
import java.util.ArrayList;
import java.util.List;
import android.util.Log;
import 	java.util.BitSet;
import java.util.Collections;

import com.androidfuzzer.fuzzer.FuzzerHelper;
import com.example.ss.WifiManagerFuzzer;

/**
 * Fuzzer object that creates wifiConfiguration objects and uses them to fuzz add/update network
 * using those objects
 *
 */
public class WifiConfigInWifiManagerFuzzer implements FuzzObject{

	@Override
	public void fuzzMe(FuzzerHelper fuzzerHelper) {
		WifiConfiguration wifiConfiguration;
		Log.i(FuzzerHelper.LOG_TAG_STRING_INFO,"In WifiConfigInWifiManagerFuzzer");
		FuzzerHelper wifiConfigFuzzer = new FuzzerHelper(WifiConfiguration.class, null);
		List<Object> wifiConfigurations = new ArrayList<Object>();
		wifiConfigFuzzer.constants.addToMaxPredefObjects(BitSet.class,Collections.singletonList((Object)new BitSet()));
		wifiConfigFuzzer.constants.addToMinPredefObjects(BitSet.class,Collections.singletonList((Object)new BitSet()));
		Log.i(FuzzerHelper.LOG_TAG_STRING_INFO,"### Initializing wifiConfig objects with max values");
		for (int i=0;i<100;i++) {
			wifiConfiguration = (WifiConfiguration)wifiConfigFuzzer.getObjectOfClass();
			//Set fields of the wifiConfig object with the max values			
			wifiConfigFuzzer.setFields(wifiConfiguration, true);
			wifiConfigurations.add(wifiConfiguration);
		}
		Log.i(FuzzerHelper.LOG_TAG_STRING_INFO,"### Initializing wifiConfig objects with min values");
		for (int i=0;i<100;i++) {
			wifiConfiguration = (WifiConfiguration)wifiConfigFuzzer.getObjectOfClass();
			//Set fields of the wifiConfig object with the max values			
			wifiConfigFuzzer.setFields(wifiConfiguration, false);
			wifiConfigurations.add(wifiConfiguration);
		}
		
		fuzzerHelper.constants.addToMaxPredefObjects(WifiConfiguration.class, wifiConfigurations);
		fuzzerHelper.constants.addToMinPredefObjects(WifiConfiguration.class, wifiConfigurations);
		String[] fuzzMethods ={"addNetwork","updateNetwork"};
		for(int i=0;i<50;i++){
			fuzzerHelper.fuzzTheseMethods(true, fuzzMethods);
		}
		
	}
}
