package com.example.ss.fuzzerObjects;
import android.util.Log;
import com.androidfuzzer.fuzzer.FuzzObject;
import com.androidfuzzer.fuzzer.FuzzerHelper;
import com.example.ss.WifiManagerFuzzer;

import android.net.wifi.WifiManager;

/**
 * A fuzz object whose fuzzMe method fuzzes all the methods when wifi is enabled
 *
 */
public class WifiManagerShuffleFuzzer implements FuzzObject{

	@Override
	public void fuzzMe(FuzzerHelper fuzzerHelper) {
		Log.d(FuzzerHelper.LOG_TAG_STRING_INFO,"### Calling shuffled methods fuzzer");
		
		String[] excludedMtdNames = {"setWifiApConfiguration"};
		fuzzerHelper.fuzzAllMethodsShuffledExceptThese(true, excludedMtdNames);
		fuzzerHelper.fuzzAllMethodsShuffledExceptThese(false, excludedMtdNames);
		
		
	}

}
