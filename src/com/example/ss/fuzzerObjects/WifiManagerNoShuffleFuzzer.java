package com.example.ss.fuzzerObjects;

import com.androidfuzzer.fuzzer.FuzzObject;
import com.androidfuzzer.fuzzer.FuzzerHelper;
import com.example.ss.WifiManagerFuzzer;

import android.util.Log;

public class WifiManagerNoShuffleFuzzer implements FuzzObject{

	@Override
	public void fuzzMe(FuzzerHelper fuzzerHelper) {
		Log.d(WifiManagerFuzzer.FUZZER_TAG,"### Calling noShuffle fuzzer");
		String[] excludedMtdNames = {"setWifiApConfiguration"};
		fuzzerHelper.fuzzAllMethodsExceptThese(true, excludedMtdNames);
		fuzzerHelper.fuzzAllMethodsExceptThese(false, excludedMtdNames);
	}
}
