package com.example.ss;
import android.widget.Toast;
import au.com.ds.ef.*;
import au.com.ds.ef.call.StateHandler;

public class ToastCheck {
	
	private class FlowContext extends StatefulContext{
		private static final long serialVersionUID = 1L;
		Toast toast;
	}
	
	MainActivity mainActivityObj;
	
	//Defining States
	private final State<FlowContext> START = FlowBuilder.state();
	private final State<FlowContext> TOAST_BUILT = FlowBuilder.state();
	private final State<FlowContext> TOAST_SHOWING= FlowBuilder.state();
	private final State<FlowContext> TOAST_ERROR = FlowBuilder.state();
	
	//Defining Events
	private final Event<FlowContext> onBuild = FlowBuilder.event();
	private final Event<FlowContext> onDisplay = FlowBuilder.event();
	private final Event<FlowContext> onError = FlowBuilder.event();
	
	private EasyFlow<FlowContext> flow;
	
	private void initFlow(){
		if(flow != null){
			return;
		}
		
		flow = FlowBuilder
				.from(START).transit(
						onBuild.to(TOAST_BUILT).transit(
								onDisplay.finish(TOAST_SHOWING),
								onError.finish(TOAST_ERROR))).executor(new UiThreadExecutor());
						
	}
	
	private void bindFlow(){
		START.whenEnter(new StateHandler<ToastCheck.FlowContext>() {
			
			@Override
			public void call(State<FlowContext> arg0, FlowContext arg1)
					throws Exception {
				// TODO Auto-generated method stub
				CharSequence text = "Hello toast!";
				int duration = Toast.LENGTH_SHORT;
				arg1.toast = Toast.makeText(mainActivityObj.getApplicationContext(), text, duration);
				onBuild.trigger(arg1);
			}
		});
		
		TOAST_BUILT.whenEnter(new StateHandler<ToastCheck.FlowContext>() {
			
			@Override
			public void call(State<FlowContext> arg0, FlowContext arg1)
					throws Exception {
				// TODO Auto-generated method stub
				if(arg1.toast == null){
					onError.trigger(arg1);
				}
				else{
					onDisplay.trigger(arg1);
				}
			}
		});
		
		TOAST_SHOWING.whenEnter(new StateHandler<ToastCheck.FlowContext>() {
			
			@Override
			public void call(State<FlowContext> arg0, FlowContext arg1)
					throws Exception {
				// TODO Auto-generated method stub
				arg1.toast.show();
				System.out.println("Exiting Gracefully");
			}
		});
		
		TOAST_ERROR.whenEnter(new StateHandler<ToastCheck.FlowContext>() {

			@Override
			public void call(State<FlowContext> arg0, FlowContext arg1)
					throws Exception {
				// TODO Auto-generated method stub
				System.out.println("Exiting Disgracefully");
			}
		});
	}
	
	public void start(MainActivity mainActivityObj){
		this.mainActivityObj = mainActivityObj;
		initFlow();
		bindFlow();
		flow.start(new FlowContext());
	}
}
