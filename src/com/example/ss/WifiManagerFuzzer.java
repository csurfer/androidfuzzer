package com.example.ss;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.androidfuzzer.fuzzer.FuzzEvent;
import com.androidfuzzer.fuzzer.FuzzerHelper;
import com.example.ss.fuzzerObjects.WifiConfigInWifiManagerFuzzer;
import com.example.ss.fuzzerObjects.WifiManagerShuffleFuzzer;
import com.example.ss.fuzzerObjects.WifiManagerNoShuffleFuzzer;

import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.widget.Toast;
import au.com.ds.ef.EasyFlow;
import au.com.ds.ef.Event;
import au.com.ds.ef.FlowBuilder;
import au.com.ds.ef.State;
import au.com.ds.ef.StatefulContext;
import au.com.ds.ef.call.StateHandler;
import android.util.Log;

public class WifiManagerFuzzer {
	private WifiManager wifiManager;
	private FuzzerHelper wifiFuzzerHelper;
	public static final String FUZZER_TAG = "WifiManagerFuzzer";
	
	public WifiManagerFuzzer(WifiManager wifiManager){
		this.wifiManager = wifiManager;
		this.wifiFuzzerHelper = new FuzzerHelper(WifiManager.class, wifiManager);
	}	
	
	/* Define the states */
	private final State<FlowContext> START = FlowBuilder.state();
	private final State<FlowContext> START_FUZZ = FlowBuilder.state();
	private final State<FlowContext> CONFIG_FUZZ_EPS = FlowBuilder.state();
	private final State<FlowContext> CONFIG_FUZZ_DONE = FlowBuilder.state();
	private final State<FlowContext> END = FlowBuilder.state();

	/* Defining events */
	private final FuzzEvent onStarted = new FuzzEvent();
	private final FuzzEvent shuffledMethodsFuzz = new FuzzEvent(new WifiManagerShuffleFuzzer());
	private final FuzzEvent nonShuffledMethodsFuzz = new FuzzEvent(new WifiManagerNoShuffleFuzzer());
	private final FuzzEvent wifiConfigFuzz = new FuzzEvent(new WifiConfigInWifiManagerFuzzer());
	private final FuzzEvent epsilon1 = new FuzzEvent();
	private final FuzzEvent epsilon2 = new FuzzEvent();
	private final FuzzEvent epsilon3 = new FuzzEvent();
	private final FuzzEvent onConfigFuzzingDone = new FuzzEvent();
	
	private EasyFlow<FlowContext> wifiManagerFuzzerFlow;

	private void initFlow(){
		wifiManagerFuzzerFlow = FlowBuilder.from(START).transit(
				onStarted.to(START_FUZZ).transit(
						shuffledMethodsFuzz.to(CONFIG_FUZZ_EPS).transit(epsilon1.to(START_FUZZ).transit(
						nonShuffledMethodsFuzz.to(CONFIG_FUZZ_EPS).transit(epsilon2.to(START_FUZZ).transit(
						wifiConfigFuzz.to(CONFIG_FUZZ_EPS).transit(epsilon3.to(START_FUZZ).transit(
						onConfigFuzzingDone.finish(END)))))))))
							.executor(new UiThreadExecutor());
	}
	
	private void bindFlow(){
		START.whenEnter(new StateHandler<FlowContext>() {
			
			@Override
			public void call(State<FlowContext> arg0, FlowContext context)
					throws Exception {
				wifiManager.setWifiEnabled(true);
				Log.d(FUZZER_TAG,"Setting wifi enabled");
				onStarted.trigger(context);
			}
		});
		
		START_FUZZ.whenEnter(new StateHandler<FlowContext>() {

			@Override
			public void call(State<FlowContext> arg0, FlowContext context){	
				int rand = new Random().nextInt(4);
				Log.d(FUZZER_TAG,"Random number generated:"+rand);
				switch(rand){
				case 0:
					shuffledMethodsFuzz.fuzz(wifiFuzzerHelper);
					shuffledMethodsFuzz.trigger(context);
					break;
				case 1:
					nonShuffledMethodsFuzz.fuzz(wifiFuzzerHelper);
					nonShuffledMethodsFuzz.trigger(context);
					break;
				case 2:
					wifiConfigFuzz.fuzz(wifiFuzzerHelper);
					wifiConfigFuzz.trigger(context);
					break;
				case 3:
					onConfigFuzzingDone.trigger(context);
					break;
				default:
					break;
				}
				
			}
		});
		
		CONFIG_FUZZ_DONE.whenEnter(new StateHandler<FlowContext>(){

			@Override
			public void call(State<FlowContext> arg0, FlowContext context)
					throws Exception {
				Log.e(FUZZER_TAG,"Config fuzz done");
				wifiManager.setWifiEnabled(false);
				onConfigFuzzingDone.trigger(context);
			}
			
		});
		
		CONFIG_FUZZ_EPS.whenEnter(new StateHandler<FlowContext>(){

			@Override
			public void call(State<FlowContext> arg0, FlowContext context)
					throws Exception {
				epsilon1.trigger(context);
				epsilon2.trigger(context);
				epsilon3.trigger(context);
			}
			
		});
		
		END.whenEnter(new StateHandler<FlowContext>() {

			@Override
			public void call(State<FlowContext> arg0, FlowContext arg1)
					throws Exception {
				Log.d(FUZZER_TAG,"Done");
			}
			
		});
	}
	
	public void fuzzMe(){
		initFlow();
		bindFlow();
		wifiManagerFuzzerFlow.start(new FlowContext());
		Log.e("wifiFuzzer","Returning from fuzzMe");
	}
}
