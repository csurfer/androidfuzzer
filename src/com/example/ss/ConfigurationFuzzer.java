package com.example.ss;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pManager.Channel;
import android.os.Looper;
import android.content.BroadcastReceiver;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import android.util.Log;

@SuppressLint("NewApi")
public class ConfigurationFuzzer {

	WifiP2pManager wifiP2pManager;
	Channel channel;

	public ConfigurationFuzzer(MainActivity mainActivityObj) {
		IntentFilter intentFilter = new IntentFilter(
				WifiManager.WIFI_STATE_CHANGED_ACTION);
		intentFilter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
		intentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
		intentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
		intentFilter
				.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
		intentFilter
				.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);
		/* Register broadcast receiver */
		mainActivityObj.registerReceiver(new MyBroadcastListener(),
				intentFilter);
	}

	public void fuzzAllManagers(Context context, MainActivity mainActivityObj) {
		WifiManager wifiManager = (WifiManager) context
				.getSystemService(Context.WIFI_SERVICE);
		wifiP2pManager = (WifiP2pManager) context
				.getSystemService(Context.WIFI_P2P_SERVICE);
		new WifiManagerFuzzer(wifiManager).fuzzMe();
		
		//fuzzWifiConfiguration(wifiManager);
		//wifiApConfigBug(wifiManager);
		//fuzzWifiP2pConfiguration(context, wifiP2pManager);
		
		try {
			Thread.sleep(8000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void fuzzWifiConfiguration(WifiManager wifiManager) {
		wifiManager.setWifiEnabled(true);
		List<WifiConfiguration> wifiConfigs = wifiManager
				.getConfiguredNetworks();

		WifiConfiguration newWifiConfig = wifiConfigs.get(0);
		newWifiConfig.SSID = "\"soujanya\"";
		int newNetworkId = wifiManager.addNetwork(newWifiConfig);
		Log.e("error", "added network id:" + newNetworkId);
		wifiManager.enableNetwork(newNetworkId, true);
		for (WifiConfiguration wifi : wifiManager.getConfiguredNetworks()) {
			Log.e("error", "1 WifiConfig SSID :" + wifi.SSID);
		}
		wifiManager.disableNetwork(newNetworkId);
		wifiManager.removeNetwork(newNetworkId);
		for (WifiConfiguration wifi : wifiManager.getConfiguredNetworks()) {
			Log.e("error", "2 WifiConfig SSID :" + wifi.SSID);
		}
	}
	
	private void wifiApConfigBug(WifiManager wifiManager){
		WifiConfiguration wifiConfig = new WifiConfiguration();
		wifiConfig.SSID = null;
		Class c;
		try {
			c = Class.forName("android.net.wifi.WifiManager");
			Method m = c.getMethod("setWifiApConfiguration", new Class[]{WifiConfiguration.class});
			m.invoke(wifiManager, wifiConfig);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
	}

	private void fuzzWifiP2pConfiguration(Context context,
			WifiP2pManager wifiP2pManager) {
		channel = wifiP2pManager.initialize(context, Looper.getMainLooper(),
				null);
		wifiP2pManager.discoverPeers(channel, new P2pActionListener());
	}

	/*
	 * Discover peer callback
	 */
	private class P2pActionListener implements WifiP2pManager.ActionListener {
		@Override
		public void onSuccess() {
			Log.i("manager", "Peer discovery successful");
			wifiP2pManager.requestPeers(channel, new P2pPeerListListener());
		}

		@Override
		public void onFailure(int reason) {
			Log.e("manager", "Peer discovery failed: Reason code" + reason);
			wifiP2pManager.discoverPeers(channel, this);
		}
	}

	/**
	 * Request peers callback
	 */
	private class P2pPeerListListener implements
			WifiP2pManager.PeerListListener {

		@Override
		public void onPeersAvailable(WifiP2pDeviceList peers) {
			Log.e("manager", "Contents" + peers.toString());
			Log.e("manager", "Peers identified " + peers.getDeviceList().size());
			for (WifiP2pDevice wifiP2ppeer : peers.getDeviceList()) {
				Log.e("manager", "Device " + wifiP2ppeer.deviceName);
			}
		}
	}

	private class MyBroadcastListener extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {
				int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE,
						-1);
				if (state == WifiP2pManager.WIFI_P2P_STATE_ENABLED) {
					Log.e("error", "P2p Enabled");
				} else {
					Log.e("error", "P2p Disabled");
				}
			} else if (WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)) {
				if (wifiP2pManager != null) {
					// wifiP2pManager.requestPeers(channel, new
					// P2pPeerListListener());
				}
			}

		}

	}

}
